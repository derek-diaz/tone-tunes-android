package com.gigasage.tonetunes;

import java.util.ArrayList;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.ToneGenerator;

public class Player implements Runnable {
	//private String song = "";
	private int typePlayer;
	private Thread thread = null;
	private MediaPlayer key0, key1, key2, key3, key4, key5, key6, key7, key8, key9, keyp, keys;
	private ToneGenerator tone = new ToneGenerator(AudioManager.STREAM_MUSIC, 100);
	ArrayList<String> song = new ArrayList<String>();
	ProgressDialog dialog = null;
	private int status = 0;
	
	public Player (int playerType){
		
		
		if (playerType == 0 ){
			this.typePlayer = 0;
		}else {
			this.typePlayer = 1;
		}
		
	}

	public void setupSamsung(MediaPlayer skey0, MediaPlayer skey1, MediaPlayer skey2, MediaPlayer skey3, MediaPlayer skey4, MediaPlayer skey5, MediaPlayer skey6, MediaPlayer skey7, MediaPlayer skey8, MediaPlayer skey9, MediaPlayer skeys, MediaPlayer skeyp) {
		key0 = skey0;
        key1 = skey1;
        key2 = skey2;
        key3 = skey3;
        key4 = skey4;
        key5 = skey5;
        key6 = skey6;
        key7 = skey7;
        key8 = skey8;
        key9 = skey9;
        keyp = skeyp;
        keys = skeys;	
	}

	
	
	public void setSong(String newSong){
		String delims = "[ ]+";
		String [] songArr = newSong.split(delims);
		//For each en java
		for(String songPice : songArr){
			song.add(songPice);
		}		
	}
	
	private void playTone(String i){
		if (status == 1){
		if (i.equals("0")){
			if (typePlayer == 0){
				tone.startTone(ToneGenerator.TONE_DTMF_0);
			}else{
				key0.seekTo(0);	
		    	key0.start();
			}
		}
		if (i.equals("1")){
			if (typePlayer == 0){
				tone.startTone(ToneGenerator.TONE_DTMF_1);
			}else{
				key1.seekTo(0);	
		    	key1.start();
			}
		}
		if (i.equals("2")){
			if (typePlayer == 0){
				tone.startTone(ToneGenerator.TONE_DTMF_2);
			}else{
				key2.seekTo(0);	
		    	key2.start();
			}
		}
		
		if (i.equals("3")){
			if (typePlayer == 0){
				tone.startTone(ToneGenerator.TONE_DTMF_3);
			}else{
				key3.seekTo(0);	
		    	key3.start();
			}
		}
		
		if (i.equals("4")){
			if (typePlayer == 0){
				tone.startTone(ToneGenerator.TONE_DTMF_4);
			}else{
				key4.seekTo(0);	
		    	key4.start();
			}
		}
		
		if (i.equals("5")){
			if (typePlayer == 0){
				tone.startTone(ToneGenerator.TONE_DTMF_5);
			}else{
				key5.seekTo(0);	
		    	key5.start();
			}
		}
		
		if (i.equals("6")){
			if (typePlayer == 0){
				tone.startTone(ToneGenerator.TONE_DTMF_6);
			}else{
				key6.seekTo(0);	
		    	key6.start();
			}
		}
		
		if (i.equals("7")){
			if (typePlayer == 0){
				tone.startTone(ToneGenerator.TONE_DTMF_7);
			}else{
				key7.seekTo(0);	
		    	key7.start();
			}
		}
		
		if (i.equals("8")){
			if (typePlayer == 0){
				tone.startTone(ToneGenerator.TONE_DTMF_8);
			}else{
				key8.seekTo(0);	
		    	key8.start();
			}
		}
		
		if (i.equals("9")){
			if (typePlayer == 0){
				tone.startTone(ToneGenerator.TONE_DTMF_9);
			}else{
				key9.seekTo(0);	
		    	key9.start();
			}
		}
		if (i.equals("*")){
			if (typePlayer == 0){
				tone.startTone(ToneGenerator.TONE_DTMF_S);
			}else{
				keys.seekTo(0);	
		    	keys.start();
			}
		}
		
		if (i.equals("#")){
			if (typePlayer == 0){
				tone.startTone(ToneGenerator.TONE_DTMF_P);
			}else{
				keyp.seekTo(0);	
		    	keyp.start();
			}
		}
	  }//status
	}
	
	public void stopTone(){
		if (typePlayer == 0){
		tone.stopTone();}
	}
	
	public ProgressDialog dialogRet(){
		return dialog;
	}

	
	public void playSong(){
		this.thread = null;
		this.thread = new Thread(this);
		thread.start();
		
	}
	
	public int getStatus(){
		return this.status;
	}
	
	public void setDialog(ProgressDialog s){
		dialog = s;
		dialog.setCancelable(true);
		dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {

             stopThread();
            }
        });
	}
	
	public void stopThread(){
		if (status == 1){
			status = 0;
			thread.interrupt();
			stopTone();
			dialog.dismiss();
		}
	}
		


	@Override
	public void run() {
			    try {
			    int length = song.size();
			   
			    if (status == 0){
			    	status = 1;
				int count = 0;
			      while (count < length) {

			    	playTone (song.get(count));
			    	
			        Thread.sleep(250L);
			        
			        stopTone();
			        
			        count++;
			      }
			      status = 0;
			      dialog.dismiss();

			      }// status check
			    } catch (InterruptedException iex) {}
		
	}

}
