package com.gigasage.tonetunes;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.SubMenu;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;


public class samsung extends SherlockActivity{

	MediaPlayer mp, skey0, skey1, skey2, skey3, skey4, skey5, skey6, skey7, skey8, skey9, skeyp, skeys, navi;
	int check = 0, song;
	private Vibrator vibrator; 
	ProgressDialog dialog = null;
	TextView txtSong;
	TextView txtSongTitle;
 
	Player player = new Player(1);
	

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.samsung);
        this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
        
      pad(); 

    }// onCreate
    
 private void pad() {
	  //ActionBar Settings
     getSupportActionBar().setDisplayShowHomeEnabled(true);
     getSupportActionBar().setDisplayHomeAsUpEnabled(true);
     getSupportActionBar().setHomeButtonEnabled(true);
     getSupportActionBar().setBackgroundDrawable(new ColorDrawable(0xff2b2b2b));
     getSupportActionBar().setTitle(Html.fromHtml("<font color='#00e111'>S PAD</font>"));

//Initializes all the pad buttons       
     initializePad();
   
//Get song selection
 	Bundle b = getIntent().getExtras();
 	song = b.getInt("song", 0);    
     
//Declarations
//---------------------------------------------------------------------------------
     
     Button key0 = (Button) findViewById(R.id.key0s);
     Button key1 = (Button) findViewById(R.id.key1s);
     Button key2 = (Button) findViewById(R.id.key2s);
     Button key3 = (Button) findViewById(R.id.key3s);
     Button key4 = (Button) findViewById(R.id.key4s);
     Button key5 = (Button) findViewById(R.id.key5s);
     Button key6 = (Button) findViewById(R.id.key6s);
     Button key7 = (Button) findViewById(R.id.key7s);
     Button key8 = (Button) findViewById(R.id.key8s);
     Button key9 = (Button) findViewById(R.id.key9s);
     Button keyAsterik = (Button) findViewById(R.id.keyasks);
     Button keyPound = (Button) findViewById(R.id.keypounds);
     txtSong = (TextView)findViewById(R.id.song);
    
//Song Gallery
//-----------------------------------------------------------------------------------------------------
		setSong();

//Set onTouch      
//------------------------------------------------------------------------------------------------------
     key0.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				switch (event.getAction() ) { 
				 case MotionEvent.ACTION_DOWN:
					 playKey('0');
			    	 break;
			    }
				return false;
			}
     });
     
     key1.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				switch (event.getAction() ) { 
				 case MotionEvent.ACTION_DOWN:
					 playKey('1');
			    	 break;
			    }
				return false;
			}
     });

     key2.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				switch (event.getAction() ) { 
				 case MotionEvent.ACTION_DOWN:
					 playKey('2');
			    	 break;
			    }
				return false;
			}
     });
     
     key3.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				
				switch (event.getAction() ) { 
				 case MotionEvent.ACTION_DOWN:
					 playKey('3');
			    	 break;
			    }
				return false;
			}
     });
     
     key4.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				 
				switch (event.getAction() ) { 
				 case MotionEvent.ACTION_DOWN:
					 playKey('4');
			    	 break;
			    }
				return false;
			}
     });
     
     key5.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				 
				switch (event.getAction() ) { 
				 case MotionEvent.ACTION_DOWN:
					 playKey('5');
			    	 break;
			    }
				return false;
			}
     });
     
     key6.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				 
				switch (event.getAction() ) { 
				 case MotionEvent.ACTION_DOWN:
					 playKey('6');
			    	 break;
			    }
				return false;
			}
     });
     
     key7.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				 
				switch (event.getAction() ) { 
				 case MotionEvent.ACTION_DOWN:
					 playKey('7');;
			    	 break;
			    }
				return false;
			}
     });
     
     key8.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				 
				switch (event.getAction() ) { 
				 case MotionEvent.ACTION_DOWN:
					 playKey('8');
			    	 break;
			    }
				return false;
			}
     });
     
     key9.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				 
				switch (event.getAction() ) { 
				 case MotionEvent.ACTION_DOWN:
					 playKey('9');
			    	 break;
			    }
				return false;
			}
     });
     
     keyPound.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				 
				switch (event.getAction() ) { 
				 case MotionEvent.ACTION_DOWN:
					 playKey('p');
			    	 break;
			    }
				return false;
			}
     });
     
     keyAsterik.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				 
				switch (event.getAction() ) { 
				 case MotionEvent.ACTION_DOWN:
					 playKey('a');
			    	 break;
			    }
				return false;
			}
     });
		
	}

@Override
    public void onConfigurationChanged(Configuration newConfig) {
      super.onConfigurationChanged(newConfig);
      setContentView(R.layout.samsung);
      this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
      
      pad();
      
    }
    
   
 //Methods------------------------------------------------------------------------------------------
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
    	Log.w("back","pressed");
        if (keyCode == KeyEvent.KEYCODE_BACK && player.getStatus() == 1) {
           player.stopThread();
           //Log.w("back","pressed");
        }
        
        //Hardware keyboard support
        switch(keyCode){
        case KeyEvent.KEYCODE_0:
        	playKey('0');
        	break;
        case KeyEvent.KEYCODE_1:
        	playKey('1');
        	break;
        case KeyEvent.KEYCODE_2:
        	playKey('2');
        	break;
        case KeyEvent.KEYCODE_3:
        	playKey('3');
        	break;
        case KeyEvent.KEYCODE_4:
        	playKey('4');
        	break;
        case KeyEvent.KEYCODE_5:
        	playKey('5');
        	break;
        case KeyEvent.KEYCODE_6:
        	playKey('6');
        	break;
        case KeyEvent.KEYCODE_7:
        	playKey('7');
        	break;
        case KeyEvent.KEYCODE_8:
        	playKey('8');
        	break;
        case KeyEvent.KEYCODE_9:
        	playKey('9');
        	break;
        case KeyEvent.KEYCODE_S:
        	playKey('a');
        	break;
        case KeyEvent.KEYCODE_P:
        	playKey('p');
        	break;
        	
        }//end case for hardware keyboard support
        return super.onKeyDown(keyCode, event);
    }
    
   
    private void initializePad(){
	  skey0 = MediaPlayer.create(this.getApplicationContext(), R.raw.a0);
      skey1 = MediaPlayer.create(this.getApplicationContext(), R.raw.a1);
      skey2 = MediaPlayer.create(this.getApplicationContext(), R.raw.a2);
      skey3 = MediaPlayer.create(this.getApplicationContext(), R.raw.a3);
      skey4 = MediaPlayer.create(this.getApplicationContext(), R.raw.a4);
      skey5 = MediaPlayer.create(this.getApplicationContext(), R.raw.a5);
      skey6 = MediaPlayer.create(this.getApplicationContext(), R.raw.a6);
      skey7 = MediaPlayer.create(this.getApplicationContext(), R.raw.a7);
      skey8 = MediaPlayer.create(this.getApplicationContext(), R.raw.a8);
      skey9 = MediaPlayer.create(this.getApplicationContext(), R.raw.a9);
      skeyp = MediaPlayer.create(this.getApplicationContext(), R.raw.ap);
      skeys = MediaPlayer.create(this.getApplicationContext(), R.raw.as);
      navi = MediaPlayer.create(this.getApplicationContext(), R.raw.hey_navi);
  	  player.setupSamsung(skey0, skey1, skey2, skey3, skey4, skey5, skey6, skey7, skey8, skey9, skeys, skeyp);
  	  vibrator = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
 }
 
 
//Song Gallery
//-----------------------------------------------------------------------------------------------------
 
    private void setSong(){
	 txtSong.setMovementMethod(new ScrollingMovementMethod());
		txtSong.setScrollbarFadingEnabled(false);
		final String [] TitleArr= getResources().getStringArray(R.array.SamsungMusic);
		getSupportActionBar().setTitle(Html.fromHtml("<font color='#00e111'>" + TitleArr[song] + "</font>"));
		txtSong.scrollTo(0,0);
				if (song == 0){ // Song of Time
					txtSong.setText(R.string.song_of_time_samsung);
					player.setSong(getString(R.string.song_of_time_samsung));
				}else if(song == 1){// Tetris
					txtSong.setText(R.string.tetris_samsung);	
					player.setSong(getString(R.string.tetris_samsung));
				}else if(song == 2){//Jeopardy
					txtSong.setText(R.string.jeopardy_samsung);
					player.setSong(getString(R.string.jeopardy_samsung));
				}else if(song == 3){//Pirates
					txtSong.setText(R.string.pirates_samsung);
					player.setSong(getString(R.string.pirates_samsung));
				}else if(song == 4){//Funkytown
					txtSong.setText(R.string.funkytown_samsung);
					player.setSong(getString(R.string.funkytown_samsung));
				}else if(song == 5){//Imperial March
					txtSong.setText(R.string.imperial_march_samsung);
					player.setSong(getString(R.string.imperial_march_samsung));
				}else if(song == 6){//Flintstones
					txtSong.setText(R.string.flintstones_samsung);
					player.setSong(getString(R.string.flintstones_samsung));
				}else if(song == 7){//Keyboard Cat
					txtSong.setText(R.string.keyboard_cat_samsung);
					player.setSong(getString(R.string.keyboard_cat_samsung));
				}else if(song == 8){//Zelda's Lullaby
					txtSong.setText(R.string.zelda_samsung);
					player.setSong(getString(R.string.zelda_samsung));
					navi.seekTo(0);	
			    	navi.start();
				}else if(song == 9){//Apona's Song
					txtSong.setText(R.string.apona_samsung);
					player.setSong(getString(R.string.apona_samsung));
				}else if(song == 10){//Saria's Song
					txtSong.setText(R.string.saria_samsung);
					player.setSong(getString(R.string.saria_samsung));
				}else if(song == 11){//ode of joy's Song
					txtSong.setText(R.string.ode_samsung);
					player.setSong(getString(R.string.ode_samsung));
				}else if(song == 12){//Super mario's Song
					txtSong.setText(R.string.mario_samsung);
					player.setSong(getString(R.string.mario_samsung));
				}else if(song == 13){//Sweet Child o' Mine Song
					txtSong.setText(R.string.sweet_child_samsung);
					player.setSong(getString(R.string.sweet_child_samsung));
				}else if(song == 14){//Sonic Song
					txtSong.setText(R.string.sonic_samsung);
					player.setSong(getString(R.string.sonic_samsung));
				}else if(song == 15){// indy jones Song
					txtSong.setText(R.string.indy_jones_sansung);
					player.setSong(getString(R.string.indy_jones_sansung));
				}else if(song == 16){//schindler Song
					txtSong.setText(R.string.schindler_list_samsung);
					player.setSong(getString(R.string.schindler_list_samsung));
				}else if(song == 17){//start it Song
					txtSong.setText(R.string.start_it_samsung);
					player.setSong(getString(R.string.start_it_samsung));
				}else if(song == 18){//jingle bells Song
					txtSong.setText(R.string.jingle_bells_samsung);
					player.setSong(getString(R.string.jingle_bells_samsung));
				}else if(song == 19){//ball game Song
					txtSong.setText(R.string.ball_game);
					player.setSong(getString(R.string.ball_game));
				}else if(song == 20){//chocobo Song
					txtSong.setText(R.string.chocobo_samsung);
					player.setSong(getString(R.string.chocobo_samsung));
				}else if(song == 21){//spiderman Song
					txtSong.setText(R.string.spider_man_samsung);
					player.setSong(getString(R.string.spider_man_samsung));
				}else if(song == 22){//popeye Song
					txtSong.setText(R.string.popeye_samsung);
					player.setSong(getString(R.string.popeye_samsung));
				}else if(song == 23){//halloween Song
					txtSong.setText(R.string.haloween_samsung);
					player.setSong(getString(R.string.haloween_samsung));
				}else if(song == 24){//abc Song
					txtSong.setText(R.string.abc_samsung);
					player.setSong(getString(R.string.abc_samsung));
				}else if(song == 25){//forest gump Song
					txtSong.setText(R.string.forest_gump_samsung);
					player.setSong(getString(R.string.forest_gump_samsung));
				}else if(song == 26){//Full Metal Alchemist: Brotherhood Song
					txtSong.setText(R.string.fmab_samsung);
					player.setSong(getString(R.string.fmab_samsung));
				}else if(song == 27){//Full Metal Alchemist: Brotherhood Song
					txtSong.setText(R.string.iron_man_samsung);
					player.setSong(getString(R.string.iron_man_samsung));
				}
 }
 
 
    private void playKey(char key){
	 switch (key) { 
	 case '0':
		 skey0.seekTo(0);
		 skey0.start();
		 vibrate();
    	 break;
	 case '1':
		 skey1.seekTo(0);
		 skey1.start();
		 vibrate();
		 break;
	 case '2':
		 skey2.seekTo(0);
		 skey2.start();
		 vibrate();
		 break;
	 case '3':
		 skey3.seekTo(0);
		 skey3.start();
		 vibrate();
		 break;
	 case '4':
		 skey4.seekTo(0);
		 skey4.start();
		 vibrate();
		 break;
	 case '5':
		 skey5.seekTo(0);
		 skey5.start();
		 vibrate();
		 break;
	 case '6':
		 skey6.seekTo(0);
		 skey6.start();
		 vibrate();
		 break;
	 case '7':
		 skey7.seekTo(0);
		 skey7.start();
		 vibrate();
		 break;
	 case '8':
		 skey8.seekTo(0);
		 skey8.start();
		 vibrate();
		 break;
	 case '9':
		 skey9.seekTo(0);
		 skey9.start();
		 vibrate();
		 break;
	 case 'a':
		 skeys.seekTo(0);
		 skeys.start();
		 vibrate();
		 break;
	 case 'p':
		 skeyp.seekTo(0);
		 skeyp.start();
		 vibrate();
		 break;
    }
 }
 
 private void vibrate(){
	 if (settings.getVibration(getBaseContext())){
		 vibrator.vibrate(25);
	 }
 }
 


 
 @Override
 public boolean onCreateOptionsMenu(Menu menu) {
 	
	         menu.add(R.string.play).setIcon(R.drawable.abs__ic_go).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM| MenuItem.SHOW_AS_ACTION_WITH_TEXT);;
	    	 SubMenu subMenu1 = menu.addSubMenu("Options");
	    	 subMenu1.add(0, R.string.about_title, 0, R.string.about_title);
	    	 subMenu1.add(0, R.string.settings, 0, R.string.settings);
	    	 subMenu1.add(0, R.string.facebook, 0, R.string.facebook);
	    	 subMenu1.add(0, R.string.request, 0, R.string.request);
	    	 subMenu1.getItem().setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
	    	 subMenu1.setIcon(R.drawable.abs__ic_menu_moreoverflow_holo_dark);

     return true;

 }
 
 
 @Override
 public boolean onOptionsItemSelected(MenuItem item){
 	switch (item.getItemId()){
 	case R.string.about_title:
 		Intent i = new Intent(this, about.class);
 		startActivity(i);
 		break;
 	case R.string.settings:
 		Intent u = new Intent(this, settings.class);
 		startActivity(u);
 		break;
 	case R.string.facebook:
 		Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.facebook.com/Tone.Tunes"));
 		startActivity(browserIntent);
 		break;
 	case R.string.request:
  		Intent emailIntent = new Intent(Intent.ACTION_SEND);
         emailIntent.putExtra(Intent.EXTRA_EMAIL,  new String[]{"request@gigasage.com"});
         emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Tone Tunes Request!");
         emailIntent.setType("message/rfc822");
         startActivity(Intent.createChooser(emailIntent, "Send mail..."));
         break;
 	case android.R.id.home:
		    finish();      
		    break;
 	}
 	
 	if (item.getTitle().toString().equals("Play this tone!")) {
 		dialog = ProgressDialog.show(samsung.this, "Tone Tunes", "Please wait while your tune plays...", true);
		player.setDialog(dialog);
		dialog = player.dialogRet();
		player.playSong();
	}
 	
 	return false;
 }
   
   
    protected void onStop() {
        super.onStop();
        player.stopThread();
        skey0.release();
        skey1.release();
        skey2.release();
        skey3.release();
        skey4.release();
        skey5.release();
        skey6.release();
        skey7.release();
        skey8.release();
        skey9.release();
        skeys.release();
        skeyp.release();
        navi.release();
        vibrator.cancel();
        System.gc();
        this.finish();
        
    }
    
    protected void onPause() {
        super.onStop();
        player.stopThread();
        skey0.release();
        skey1.release();
        skey2.release();
        skey3.release();
        skey4.release();
        skey5.release();
        skey6.release();
        skey7.release();
        skey8.release();
        skey9.release();
        skeys.release();
        skeyp.release();
        navi.release();
        vibrator.cancel();
        System.gc();
        this.finish();
        
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        player.stopThread();
        skey0.release();
        skey1.release();
        skey2.release();
        skey3.release();
        skey4.release();
        skey5.release();
        skey6.release();
        skey7.release();
        skey8.release();
        skey9.release();
        skeys.release();
        skeyp.release();
        navi.release();
        vibrator.cancel();
        System.gc();
        this.finish();
    }
    
}