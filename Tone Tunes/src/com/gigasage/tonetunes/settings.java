package com.gigasage.tonetunes;

import android.content.Context;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;

public class settings extends PreferenceActivity {
	
	private static final String OPT_VIBRATION = "vibration";
	private static final boolean OPT_VIBRATION_DEF = true;
	
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.settings);
	}
	
	public static boolean getVibration(Context context){ //So WRONG!
		return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(OPT_VIBRATION, OPT_VIBRATION_DEF);
		
	}
	
}
