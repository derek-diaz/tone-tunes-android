package com.gigasage.tonetunes;

import com.actionbarsherlock.app.SherlockActivity;
import com.flurry.android.FlurryAgent;
import com.google.ads.AdRequest;
import com.google.ads.AdView;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.SubMenu; 


public class MainMenu extends SherlockActivity{

    @Override
    public void onCreate(Bundle savedInstanceState) {
    	 super.onCreate(savedInstanceState);
    	 
         //ActionBar Settings
         getSupportActionBar().setDisplayShowHomeEnabled(true);
         getSupportActionBar().setDisplayHomeAsUpEnabled(true);
         getSupportActionBar().setHomeButtonEnabled(true);
         getSupportActionBar().setBackgroundDrawable(new ColorDrawable(0xff2b2b2b));
         this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
         //getSupportActionBar().setTitle(Html.fromHtml("<font color='#00e111'>TONE TUNES</font>"));
         getSupportActionBar().setTitle(Html.fromHtml("<font color='#00e111'></font>"));
    	  
    	  
    	 setContentView(R.layout.mainmenu);

    	 // Look up the AdView as a resource and load a request.
    	 AdRequest request = new AdRequest();
    	 //request.addTestDevice(AdRequest.TEST_EMULATOR);
    	 AdView adView = (AdView)this.findViewById(R.id.ad);
    	 adView.loadAd(request);
    	 
    	 //Setup Flurry Analytics
    	 FlurryAgent.onStartSession(this, "4T1CFIGNWWRVG8EUGD49");
    	 
    	 ImageView btnDtmf = (ImageView)findViewById(R.id.BTNdtmf);
    	 ImageView btnSamsung = (ImageView)findViewById(R.id.BTNsamsung);
    	

         btnSamsung.setOnTouchListener(new View.OnTouchListener() {
  			@Override
  			public boolean onTouch(View v, MotionEvent event) {
  				startActivity(new Intent(MainMenu.this, musicList1.class));
  				return false;
  			}
          });    	 
    	 
    	 
         btnDtmf.setOnTouchListener(new View.OnTouchListener() {
 			@Override
 			public boolean onTouch(View v, MotionEvent event) {
 				startActivity(new Intent(MainMenu.this, musicList2.class));
 				return false;
 			}
         });
             
    	 
    }
    
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	

	    	 SubMenu subMenu1 = menu.addSubMenu("Options");
	    	 subMenu1.add(0, R.string.about_title, 0, R.string.about_title);
	    	 subMenu1.add(0, R.string.settings, 0, R.string.settings);
	    	 subMenu1.add(0, R.string.facebook, 0, R.string.facebook);
	    	 subMenu1.add(0, R.string.request, 0, R.string.request);
	    	 subMenu1.getItem().setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
	    	 subMenu1.setIcon(R.drawable.abs__ic_menu_moreoverflow_holo_dark);

        return true;
 
    }
    
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
    	switch (item.getItemId()){
    	case R.string.about_title:
    		Intent i = new Intent(this, about.class);
    		startActivity(i);
    		break;
    	case R.string.settings:
    		Intent u = new Intent(this, settings.class);
    		startActivity(u);
    		break;
    	case R.string.facebook:
    		Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.facebook.com/Tone.Tunes"));
    		startActivity(browserIntent);
    		break;
    	case R.string.request:
     		Intent emailIntent = new Intent(Intent.ACTION_SEND);
            emailIntent.putExtra(Intent.EXTRA_EMAIL,  new String[]{"request@gigasage.com"});
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Tone Tunes Request!");
            emailIntent.setType("message/rfc822");
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            break;
    	case android.R.id.home:
    		 //Ask the user if they want to quit
				@SuppressWarnings("unused")
				AlertDialog dialog = new AlertDialog.Builder(MainMenu.this)
 		        .setIcon(android.R.drawable.ic_dialog_alert)
 		        .setTitle(R.string.quit)
 		        .setMessage("Are you sure you want to quit?")
 		        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {

 		            @Override
 		            public void onClick(DialogInterface dialog, int which) {

 		                //Stop the activity
 		            	 finish();     
 		            }

 		        })
 		        .setNegativeButton("No", null)
 		        .show();		
				break;
    	}
    	return false;
    }
   
    @Override
    public void onStop()
    {
       super.onStop();
       FlurryAgent.onEndSession(this);
    }
	
	


	
}
