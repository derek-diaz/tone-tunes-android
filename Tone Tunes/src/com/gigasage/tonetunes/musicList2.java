//Derek Diaz Correa
//Gigasge - Tone Tunes
//DTMF Song list with Ad
package com.gigasage.tonetunes;

import com.actionbarsherlock.app.SherlockListActivity;
import com.actionbarsherlock.view.SubMenu;
import com.google.ads.AdRequest;
import com.google.ads.AdView;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public class musicList2 extends SherlockListActivity {
	MediaPlayer hey;
	   @Override
	    public void onCreate(Bundle savedInstanceState) {
	    	 super.onCreate(savedInstanceState);
	    	 setContentView(R.layout.musiclist);
	    	 
	         //ActionBar Settings
	         getSupportActionBar().setDisplayShowHomeEnabled(true);
	         getSupportActionBar().setDisplayHomeAsUpEnabled(true);
	         getSupportActionBar().setHomeButtonEnabled(true);
	         getSupportActionBar().setBackgroundDrawable(new ColorDrawable(0xff2b2b2b));
	         this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
	         getSupportActionBar().setTitle(Html.fromHtml("<font color='#00e111'>SONG LIST</font>")); 
	    	 
	   //load ad
	    	 // Look up the AdView as a resource and load a request.
	    	 AdRequest request = new AdRequest();
	    	 //request.addTestDevice(AdRequest.TEST_EMULATOR);
	    	 AdView adView = (AdView)this.findViewById(R.id.ad);
	    	 adView.loadAd(request);
	    	
	         //android.R.layout.simple_list_item_1
	    	 final String [] dtmfMusicTitle= getResources().getStringArray(R.array.PhoneMusic);
	    	 setListAdapter(new ArrayAdapter<String>(this, R.layout.line , dtmfMusicTitle));
	    	 getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
	    	 getListView().setTextFilterEnabled(true);
	    	 
	    	 getListView().setOnItemClickListener(new OnItemClickListener()
	    	 {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int itemId, long arg3) {
					
					Intent intent = new Intent(musicList2.this, Dtmf.class);
					Bundle b = new Bundle();
					b.putInt("song", itemId);
					intent.putExtras(b);
					startActivity(intent);
				
					
				}
	    		 
	    	 });
	   
	   }//OnCreate
	   
	   @Override
	    public boolean onCreateOptionsMenu(Menu menu) {
	    	

		    	 SubMenu subMenu1 = menu.addSubMenu("Options");
		    	 subMenu1.add(0, R.string.about_title, 0, R.string.about_title);
		    	 subMenu1.add(0, R.string.settings, 0, R.string.settings);
		    	 subMenu1.add(0, R.string.facebook, 0, R.string.facebook);
		    	 subMenu1.add(0, R.string.request, 0, R.string.request);
		    	 subMenu1.getItem().setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
		    	 subMenu1.setIcon(R.drawable.abs__ic_menu_moreoverflow_holo_dark);

	        return true;
	 
	    }
	    
	    
	    @Override
	    public boolean onOptionsItemSelected(MenuItem item){
	    	switch (item.getItemId()){
	    	case R.string.about_title:
	    		Intent i = new Intent(this, about.class);
	    		startActivity(i);
	    		break;
	    	case R.string.settings:
	    		Intent u = new Intent(this, settings.class);
	    		startActivity(u);
	    		break;
	    	case R.string.facebook:
	    		Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.facebook.com/Tone.Tunes"));
	    		startActivity(browserIntent);
	    		break;
	    	case R.string.request:
	     		Intent emailIntent = new Intent(Intent.ACTION_SEND);
	            emailIntent.putExtra(Intent.EXTRA_EMAIL,  new String[]{"request@gigasage.com"});
	            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Tone Tunes Request!");
	            emailIntent.setType("message/rfc822");
	            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
	            break;
	    	case android.R.id.home:
	 		    finish();      
	 		    break;
	    	}
	    	return false;
	    }
	

}
