package com.gigasage.tonetunes;

import com.actionbarsherlock.app.SherlockActivity;


import com.actionbarsherlock.view.SubMenu;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.ToneGenerator;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public class Dtmf extends SherlockActivity{

	ToneGenerator tone = new ToneGenerator(AudioManager.STREAM_MUSIC, 100);
	int check = 0, song;
	private Vibrator vibrator;
	TextView txtSong, txtSongTitle;
	Player player = new Player(0);
	ProgressDialog dialog = null;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
        
        //ActionBar Settings
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(0xff2b2b2b));
        getSupportActionBar().setTitle(Html.fromHtml("<font color='#00e111'>DTMF</font>"));
             
        
//Declarations
//---------------------------------------------------------------------------------
        
        Button key0 = (Button) findViewById(R.id.key0);
        Button key1 = (Button) findViewById(R.id.key1);
        Button key2 = (Button) findViewById(R.id.key2);
        Button key3 = (Button) findViewById(R.id.key3);
        Button key4 = (Button) findViewById(R.id.key4);
        Button key5 = (Button) findViewById(R.id.key5);
        Button key6 = (Button) findViewById(R.id.key6);
        Button key7 = (Button) findViewById(R.id.key7);
        Button key8 = (Button) findViewById(R.id.key8);
        Button key9 = (Button) findViewById(R.id.key9);
        Button keyAsterik = (Button) findViewById(R.id.keyask);
        Button keyPound = (Button) findViewById(R.id.keypound);
        txtSong=(TextView)findViewById(R.id.song);
        vibrator = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
        
      

//Receive values from bundle
//-----------------------------------------------------------------------------------------------------
        
    	Bundle b = getIntent().getExtras();

    	song = b.getInt("song", 0);        
        
       
    
//Song Gallery
//-----------------------------------------------------------------------------------------------------
        setSong();

        
        
        
//Set onTouch      
//------------------------------------------------------------------------------------------------------
        key0.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {

				switch (event.getAction() ) { 
				 case MotionEvent.ACTION_DOWN:
					 	if (check == 0){
			    	   	tone.startTone(ToneGenerator.TONE_DTMF_0);
			    	   	vibrate();
			    	   	check = 1;
					 	}
			    	 break;
			     case MotionEvent.ACTION_UP:
			    	 toneStop();
			    	 break;
			    }

				return false;
			}
        });
        
        key1.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {

				switch (event.getAction() ) { 
			    case MotionEvent.ACTION_DOWN:
			    	if (check == 0){
			    	   	tone.startTone(ToneGenerator.TONE_DTMF_1);
			    	   	vibrate();
			    	   	check = 1;
					 	}
			        break;
			    case MotionEvent.ACTION_UP:
			    	toneStop();
			        break;
			    }
				return false;
			}
        });
        
        key2.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {

				switch (event.getAction() ) { 
			    case MotionEvent.ACTION_DOWN:
			    	if (check == 0){
			    	   	tone.startTone(ToneGenerator.TONE_DTMF_2);
			    	   	vibrate();
			    	   	check = 1;
					 	}
			        break;
			    case MotionEvent.ACTION_UP:
			    	toneStop();
			        break;
			    }
				return false;
			}
        });
        
        key3.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {

				switch (event.getAction() ) { 
			    case MotionEvent.ACTION_DOWN:
			    	if (check == 0){
			    	   	tone.startTone(ToneGenerator.TONE_DTMF_3);
			    	   	vibrate();
			    	   	check = 1;
					 	}
			        break;
			    case MotionEvent.ACTION_UP:
			    	toneStop();
			        break; 
			    }
				return false;
			}
        });
        
        key4.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {

				switch (event.getAction() ) { 
			    case MotionEvent.ACTION_DOWN:
			    	if (check == 0){
			    	   	tone.startTone(ToneGenerator.TONE_DTMF_4);
			    	   	vibrate();
			    	   	check = 1;
					 	}
			        break;
			    case MotionEvent.ACTION_UP:
			    	toneStop();
			        break; 
			    }
				return false;
			}
        });
        
        key5.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {

				switch (event.getAction() ) { 
			    case MotionEvent.ACTION_DOWN:
			    	if (check == 0){
			    	   	tone.startTone(ToneGenerator.TONE_DTMF_5);
			    	   	vibrate();
			    	   	check = 1;
					 	}
			        break;
			    case MotionEvent.ACTION_UP:
			    	toneStop();
			        break; 
			    }
				return false;
			}
        });
        
        key6.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {

				switch (event.getAction() ) { 
			    case MotionEvent.ACTION_DOWN:
			    	if (check == 0){
			    	   	tone.startTone(ToneGenerator.TONE_DTMF_6);
			    	   	vibrate();
			    	   	check = 1;
					 	}
			        break;
			    case MotionEvent.ACTION_UP:
			    	toneStop();
			        break; 
			    }
				return false;
			}
        });
        
        key7.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {

				switch (event.getAction() ) { 
			    case MotionEvent.ACTION_DOWN:
			    	if (check == 0){
			    	   	tone.startTone(ToneGenerator.TONE_DTMF_7);
			    	   	vibrate();
			    	   	check = 1;
					 	};
			        break;
			    case MotionEvent.ACTION_UP:
			    	toneStop();
			        break; 
			    }
				return false;
			}
        });
        
        key8.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {

				switch (event.getAction() ) { 
			    case MotionEvent.ACTION_DOWN:
			    	if (check == 0){
			    	   	tone.startTone(ToneGenerator.TONE_DTMF_8);
			    	   	vibrate();
			    	   	check = 1;
					 	}
			        break;
			    case MotionEvent.ACTION_UP:
			    	toneStop();
			        break; 
			    }
				return false;
			}
        });
        
        key9.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {

				switch (event.getAction() ) { 
			    case MotionEvent.ACTION_DOWN:
			    	if (check == 0){
			    	   	tone.startTone(ToneGenerator.TONE_DTMF_9);
			    	   	vibrate();
			    	   	check = 1;
					 	}
			        break;
			    case MotionEvent.ACTION_UP:
			    	toneStop();
			        break; 
			    }
				return false;
			}
        });
        
        keyAsterik.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				
				switch (event.getAction() ) { 
			    case MotionEvent.ACTION_DOWN:
			    	if (check == 0){
			    	   	tone.startTone(ToneGenerator.TONE_DTMF_S);
			    	   	check = 1;
			    	   	vibrate();
					 	}
			        break;
			    case MotionEvent.ACTION_UP:
			    	toneStop();
			        break; 
			    }
				return false;
			}
        });
        
        keyPound.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {

				switch (event.getAction() ) { 
			    case MotionEvent.ACTION_DOWN:
			    	if (check == 0){
			    	   	tone.startTone(ToneGenerator.TONE_DTMF_P);
			    	   	vibrate();
			    	   	check = 1;
					 	}
			        break;
			    case MotionEvent.ACTION_UP:
			    	toneStop();
			        break; 
			    }
				return false;
			}
        });
        
    }
    
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
      super.onConfigurationChanged(newConfig);
      setContentView(R.layout.main);
      this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
      
    //ActionBar Settings
      getSupportActionBar().setDisplayShowHomeEnabled(true);
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      getSupportActionBar().setHomeButtonEnabled(true);
      getSupportActionBar().setBackgroundDrawable(new ColorDrawable(0xff2b2b2b));
      getSupportActionBar().setTitle(Html.fromHtml("<font color='#00e111'>DTMF</font>"));
           
      
//Declarations
//---------------------------------------------------------------------------------
      
      Button key0 = (Button) findViewById(R.id.key0);
      Button key1 = (Button) findViewById(R.id.key1);
      Button key2 = (Button) findViewById(R.id.key2);
      Button key3 = (Button) findViewById(R.id.key3);
      Button key4 = (Button) findViewById(R.id.key4);
      Button key5 = (Button) findViewById(R.id.key5);
      Button key6 = (Button) findViewById(R.id.key6);
      Button key7 = (Button) findViewById(R.id.key7);
      Button key8 = (Button) findViewById(R.id.key8);
      Button key9 = (Button) findViewById(R.id.key9);
      Button keyAsterik = (Button) findViewById(R.id.keyask);
      Button keyPound = (Button) findViewById(R.id.keypound);
      txtSong=(TextView)findViewById(R.id.song);
      vibrator = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
      
    

//Receive values from bundle
//-----------------------------------------------------------------------------------------------------
      
  	Bundle b = getIntent().getExtras();

  	song = b.getInt("song", 0);        
      
     
  
//Song Gallery
//-----------------------------------------------------------------------------------------------------
      setSong();

      
      
      
//Set onTouch      
//------------------------------------------------------------------------------------------------------
      key0.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {

				switch (event.getAction() ) { 
				 case MotionEvent.ACTION_DOWN:
					 	if (check == 0){
			    	   	tone.startTone(ToneGenerator.TONE_DTMF_0);
			    	   	vibrate();
			    	   	check = 1;
					 	}
			    	 break;
			     case MotionEvent.ACTION_UP:
			    	 toneStop();
			    	 break;
			    }

				return false;
			}
      });
      
      key1.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {

				switch (event.getAction() ) { 
			    case MotionEvent.ACTION_DOWN:
			    	if (check == 0){
			    	   	tone.startTone(ToneGenerator.TONE_DTMF_1);
			    	   	vibrate();
			    	   	check = 1;
					 	}
			        break;
			    case MotionEvent.ACTION_UP:
			    	toneStop();
			        break;
			    }
				return false;
			}
      });
      
      key2.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {

				switch (event.getAction() ) { 
			    case MotionEvent.ACTION_DOWN:
			    	if (check == 0){
			    	   	tone.startTone(ToneGenerator.TONE_DTMF_2);
			    	   	vibrate();
			    	   	check = 1;
					 	}
			        break;
			    case MotionEvent.ACTION_UP:
			    	toneStop();
			        break;
			    }
				return false;
			}
      });
      
      key3.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {

				switch (event.getAction() ) { 
			    case MotionEvent.ACTION_DOWN:
			    	if (check == 0){
			    	   	tone.startTone(ToneGenerator.TONE_DTMF_3);
			    	   	vibrate();
			    	   	check = 1;
					 	}
			        break;
			    case MotionEvent.ACTION_UP:
			    	toneStop();
			        break; 
			    }
				return false;
			}
      });
      
      key4.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {

				switch (event.getAction() ) { 
			    case MotionEvent.ACTION_DOWN:
			    	if (check == 0){
			    	   	tone.startTone(ToneGenerator.TONE_DTMF_4);
			    	   	vibrate();
			    	   	check = 1;
					 	}
			        break;
			    case MotionEvent.ACTION_UP:
			    	toneStop();
			        break; 
			    }
				return false;
			}
      });
      
      key5.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {

				switch (event.getAction() ) { 
			    case MotionEvent.ACTION_DOWN:
			    	if (check == 0){
			    	   	tone.startTone(ToneGenerator.TONE_DTMF_5);
			    	   	vibrate();
			    	   	check = 1;
					 	}
			        break;
			    case MotionEvent.ACTION_UP:
			    	toneStop();
			        break; 
			    }
				return false;
			}
      });
      
      key6.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {

				switch (event.getAction() ) { 
			    case MotionEvent.ACTION_DOWN:
			    	if (check == 0){
			    	   	tone.startTone(ToneGenerator.TONE_DTMF_6);
			    	   	vibrate();
			    	   	check = 1;
					 	}
			        break;
			    case MotionEvent.ACTION_UP:
			    	toneStop();
			        break; 
			    }
				return false;
			}
      });
      
      key7.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {

				switch (event.getAction() ) { 
			    case MotionEvent.ACTION_DOWN:
			    	if (check == 0){
			    	   	tone.startTone(ToneGenerator.TONE_DTMF_7);
			    	   	vibrate();
			    	   	check = 1;
					 	};
			        break;
			    case MotionEvent.ACTION_UP:
			    	toneStop();
			        break; 
			    }
				return false;
			}
      });
      
      key8.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {

				switch (event.getAction() ) { 
			    case MotionEvent.ACTION_DOWN:
			    	if (check == 0){
			    	   	tone.startTone(ToneGenerator.TONE_DTMF_8);
			    	   	vibrate();
			    	   	check = 1;
					 	}
			        break;
			    case MotionEvent.ACTION_UP:
			    	toneStop();
			        break; 
			    }
				return false;
			}
      });
      
      key9.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {

				switch (event.getAction() ) { 
			    case MotionEvent.ACTION_DOWN:
			    	if (check == 0){
			    	   	tone.startTone(ToneGenerator.TONE_DTMF_9);
			    	   	vibrate();
			    	   	check = 1;
					 	}
			        break;
			    case MotionEvent.ACTION_UP:
			    	toneStop();
			        break; 
			    }
				return false;
			}
      });
      
      keyAsterik.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				
				switch (event.getAction() ) { 
			    case MotionEvent.ACTION_DOWN:
			    	if (check == 0){
			    	   	tone.startTone(ToneGenerator.TONE_DTMF_S);
			    	   	check = 1;
			    	   	vibrate();
					 	}
			        break;
			    case MotionEvent.ACTION_UP:
			    	toneStop();
			        break; 
			    }
				return false;
			}
      });
      
      keyPound.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {

				switch (event.getAction() ) { 
			    case MotionEvent.ACTION_DOWN:
			    	if (check == 0){
			    	   	tone.startTone(ToneGenerator.TONE_DTMF_P);
			    	   	vibrate();
			    	   	check = 1;
					 	}
			        break;
			    case MotionEvent.ACTION_UP:
			    	toneStop();
			        break; 
			    }
				return false;
			}
      });
            
      
    }
    
 //Methods------------------------------------------------------------------------------------
    private void setSong(){
    	txtSong.setMovementMethod(new ScrollingMovementMethod());
		txtSong.setScrollbarFadingEnabled(false);
		final String [] TitleArr= getResources().getStringArray(R.array.PhoneMusic);
		getSupportActionBar().setTitle(Html.fromHtml("<font color='#00e111'>" + TitleArr[song] + "</font>"));
		txtSong.scrollTo(0,0);
				if (song == 0){ // Twinckle
					txtSong.setText(R.string.twikncle_dtmf);
					player.setSong(getString(R.string.twikncle_dtmf));
				}else if(song == 1){// Olimpic
					txtSong.setText(R.string.olympics_dtmf);
					player.setSong(getString(R.string.olympics_dtmf));
				}else if(song == 2){//Bingo
					txtSong.setText(R.string.bingo_dtmf);
					player.setSong(getString(R.string.bingo_dtmf));
				}else if(song == 3){//Old McDonald
					txtSong.setText(R.string.mcdonalds_dtmf);
					player.setSong(getString(R.string.mcdonalds_dtmf));
				}else if(song == 4){//Funkytown
					txtSong.setText(R.string.funkytown_dtmf);
					player.setSong(getString(R.string.funkytown_dtmf));
				}else if(song == 5){//mgs
					txtSong.setText(R.string.mgs_dtmf);
					player.setSong(getString(R.string.mgs_dtmf));
				}else if(song == 6){//Row Boat
					txtSong.setText(R.string.row_boat_dtmf);
					player.setSong(getString(R.string.row_boat_dtmf));
				}else if(song == 7){//ABCD
					txtSong.setText(R.string.abc_dtmf);
					player.setSong(getString(R.string.abc_dtmf));
				}else if(song == 8){//Eye of the Tiger
					txtSong.setText(R.string.eye_of_tiger_dtmf);
					player.setSong(getString(R.string.eye_of_tiger_dtmf));
				}
    }//end set song
    
    //Hardware Keyboard Support - onPress
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
    	Log.w("back","pressed");
        if (keyCode == KeyEvent.KEYCODE_BACK && player.getStatus() == 1) {
           player.stopThread();
           Log.w("back","pressed");
        }
    	
        //Hardware keyboard support
        switch(keyCode){
        case KeyEvent.KEYCODE_0:
        	if (check == 0){
	    	   	tone.startTone(ToneGenerator.TONE_DTMF_0);
	    	   	vibrate();
	    	   	check = 1;
			 	}
        	break;
        case KeyEvent.KEYCODE_1:
        	if (check == 0){
	    	   	tone.startTone(ToneGenerator.TONE_DTMF_1);
	    	   	vibrate();
	    	   	check = 1;
			 	}
        	break;
        case KeyEvent.KEYCODE_2:
        	if (check == 0){
	    	   	tone.startTone(ToneGenerator.TONE_DTMF_2);
	    	   	vibrate();
	    	   	check = 1;
			 	}
        	break;
        case KeyEvent.KEYCODE_3:
        	if (check == 0){
	    	   	tone.startTone(ToneGenerator.TONE_DTMF_3);
	    	   	vibrate();
	    	   	check = 1;
			 	}
        	break;
        case KeyEvent.KEYCODE_4:
        	if (check == 0){
	    	   	tone.startTone(ToneGenerator.TONE_DTMF_4);
	    	   	vibrate();
	    	   	check = 1;
			 	}
        	break;
        case KeyEvent.KEYCODE_5:
        	if (check == 0){
	    	   	tone.startTone(ToneGenerator.TONE_DTMF_5);
	    	   	vibrate();
	    	   	check = 1;
			 	}
        	break;
        case KeyEvent.KEYCODE_6:
        	if (check == 0){
	    	   	tone.startTone(ToneGenerator.TONE_DTMF_6);
	    	   	vibrate();
	    	   	check = 1;
			 	}
        	break;
        case KeyEvent.KEYCODE_7:
        	if (check == 0){
	    	   	tone.startTone(ToneGenerator.TONE_DTMF_7);
	    	   	vibrate();
	    	   	check = 1;
			 	}
        	break;
        case KeyEvent.KEYCODE_8:
        	if (check == 0){
	    	   	tone.startTone(ToneGenerator.TONE_DTMF_8);
	    	   	vibrate();
	    	   	check = 1;
			 	}
        	break;
        case KeyEvent.KEYCODE_9:
        	if (check == 0){
	    	   	tone.startTone(ToneGenerator.TONE_DTMF_9);
	    	   	vibrate();
	    	   	check = 1;
			 	}
        	break;
        case KeyEvent.KEYCODE_S:
        	if (check == 0){
	    	   	tone.startTone(ToneGenerator.TONE_DTMF_S);
	    	   	vibrate();
	    	   	check = 1;
			 	}
        	break;
        case KeyEvent.KEYCODE_P:
        	if (check == 0){
	    	   	tone.startTone(ToneGenerator.TONE_DTMF_P);
	    	   	vibrate();
	    	   	check = 1;
			 	}
        	break;
        	
        }//end case for hardware keyboard support
        return super.onKeyDown(keyCode, event);
    }
    
    //Hardware keyboard support - onKey up
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        
        //Hardware keyboard support
        switch(keyCode){
        case KeyEvent.KEYCODE_0:
        	toneStop();
        	break;
        case KeyEvent.KEYCODE_1:
        	toneStop();
        	break;
        case KeyEvent.KEYCODE_2:
        	toneStop();
        	break;
        case KeyEvent.KEYCODE_3:
        	toneStop();
        	break;
        case KeyEvent.KEYCODE_4:
        	toneStop();
        	break;
        case KeyEvent.KEYCODE_5:
        	toneStop();
        	break;
        case KeyEvent.KEYCODE_6:
        	toneStop();
        	break;
        case KeyEvent.KEYCODE_7:
        	toneStop();
        	break;
        case KeyEvent.KEYCODE_8:
        	toneStop();
        	break;
        case KeyEvent.KEYCODE_9:
        	toneStop();
        	break;
        case KeyEvent.KEYCODE_S:
        	toneStop();
        	break;
        case KeyEvent.KEYCODE_P:
        	toneStop();
        	break;
        	
        }//end case for hardware keyboard support
        return super.onKeyUp(keyCode, event);
    }
    
    private void toneStop(){
    	tone.stopTone();
    	check = 0;
    }
    
    private void vibrate(){
    	if (settings.getVibration(getBaseContext())){
   		 vibrator.vibrate(25);
   	 	}
    }

    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	
   	         menu.add(R.string.play).setIcon(R.drawable.abs__ic_go).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM| MenuItem.SHOW_AS_ACTION_WITH_TEXT);;
   	    	 SubMenu subMenu1 = menu.addSubMenu("Options");
   	    	 subMenu1.add(0, R.string.about_title, 0, R.string.about_title);
   	    	 subMenu1.add(0, R.string.settings, 0, R.string.settings);
   	    	 subMenu1.add(0, R.string.facebook, 0, R.string.facebook);
   	    	 subMenu1.add(0, R.string.request, 0, R.string.request);
   	    	 subMenu1.getItem().setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
   	    	 subMenu1.setIcon(R.drawable.abs__ic_menu_moreoverflow_holo_dark);

        return true;

    }
    
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
    	switch (item.getItemId()){
    	case R.string.about_title:
    		Intent i = new Intent(this, about.class);
    		startActivity(i);
    		break;
    	case R.string.settings:
    		Intent u = new Intent(this, settings.class);
    		startActivity(u);
    		break;
    	case R.string.facebook:
    		Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.facebook.com/Tone.Tunes"));
    		startActivity(browserIntent);
    		break;
    	case R.string.request:
     		Intent emailIntent = new Intent(Intent.ACTION_SEND);
            emailIntent.putExtra(Intent.EXTRA_EMAIL,  new String[]{"request@gigasage.com"});
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Tone Tunes Request!");
            emailIntent.setType("message/rfc822");
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            break;
    	case android.R.id.home:
   		    finish();      
   		    break;
    	}
    	
    	if (item.getTitle().toString().equals("Play this tone!")) {
    		dialog = ProgressDialog.show(Dtmf.this, "Tone Tunes", "Please wait while your tune plays...", true);
    		player.setDialog(dialog);
    		dialog = player.dialogRet();
    		player.playSong();
    	}
    	
    	return false;
    }
    
    @Override
    protected void onPause() {
        super.onStop();
        tone.release();
        vibrator.cancel();
        player.stopThread();
        System.gc();
        this.finish();     
    }  
    
    @Override
    protected void onStop() {
        super.onStop();
        player.stopThread();
        tone.release();
        vibrator.cancel();
        System.gc();
        this.finish();    
    }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
        tone.release();
        vibrator.cancel();
        player.stopThread();
        System.gc();
        this.finish();
    }
    
}